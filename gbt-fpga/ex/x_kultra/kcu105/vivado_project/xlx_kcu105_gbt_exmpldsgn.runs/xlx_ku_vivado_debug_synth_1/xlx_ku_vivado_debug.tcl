# 
# Synthesis run script generated by Vivado
# 

set TIME_start [clock seconds] 
proc create_report { reportName command } {
  set status "."
  append status $reportName ".fail"
  if { [file exists $status] } {
    eval file delete [glob $status]
  }
  send_msg_id runtcl-4 info "Executing : $command"
  set retval [eval catch { $command } msg]
  if { $retval != 0 } {
    set fp [open $status w]
    close $fp
    send_msg_id runtcl-5 warning "$msg"
  }
}
set_param project.vivado.isBlockSynthRun true
set_msg_config -msgmgr_mode ooc_run
create_project -in_memory -part xcku040-ffva1156-2-e

set_param project.singleFileAddWarning.threshold 0
set_param project.compositeFile.enableAutoGeneration 0
set_param synth.vivado.isSynthRun true
set_msg_config -source 4 -id {IP_Flow 19-2162} -severity warning -new_severity info
set_property webtalk.parent_dir E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.cache/wt [current_project]
set_property parent.project_path E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.xpr [current_project]
set_property XPM_LIBRARIES {XPM_CDC XPM_MEMORY} [current_project]
set_property default_lib xil_defaultlib [current_project]
set_property target_language VHDL [current_project]
set_property board_part xilinx.com:kcu105:part0:1.1 [current_project]
set_property ip_cache_permissions disable [current_project]
read_ip -quiet E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/core_sources/chipscope_ila/xlx_ku_vivado_debug.xci
set_property used_in_synthesis false [get_files -all e:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/core_sources/chipscope_ila/ila_v6_2/constraints/ila_impl.xdc]
set_property used_in_implementation false [get_files -all e:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/core_sources/chipscope_ila/ila_v6_2/constraints/ila_impl.xdc]
set_property used_in_implementation false [get_files -all e:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/core_sources/chipscope_ila/ila_v6_2/constraints/ila.xdc]
set_property used_in_implementation false [get_files -all e:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/core_sources/chipscope_ila/xlx_ku_vivado_debug_ooc.xdc]

# Mark all dcp files as not used in implementation to prevent them from being
# stitched into the results of this synthesis run. Any black boxes in the
# design are intentionally left as such for best results. Dcp files will be
# stitched into the design at a later time, either when this synthesis run is
# opened, or when it is stitched into a dependent implementation run.
foreach dcp [get_files -quiet -all -filter file_type=="Design\ Checkpoint"] {
  set_property used_in_implementation false $dcp
}
read_xdc dont_touch.xdc
set_property used_in_implementation false [get_files dont_touch.xdc]
set_param ips.enableIPCacheLiteLoad 1
close [open __synthesis_is_running__ w]

synth_design -top xlx_ku_vivado_debug -part xcku040-ffva1156-2-e -mode out_of_context

rename_ref -prefix_all xlx_ku_vivado_debug_

# disable binary constraint mode for synth run checkpoints
set_param constraints.enableBinaryConstraints false
write_checkpoint -force -noxdef xlx_ku_vivado_debug.dcp
create_report "xlx_ku_vivado_debug_synth_1_synth_report_utilization_0" "report_utilization -file xlx_ku_vivado_debug_utilization_synth.rpt -pb xlx_ku_vivado_debug_utilization_synth.pb"

if { [catch {
  write_verilog -force -mode synth_stub E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug_stub.v
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create a Verilog synthesis stub for the sub-design. This may lead to errors in top level synthesis of the design. Error reported: $_RESULT"
}

if { [catch {
  write_vhdl -force -mode synth_stub E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug_stub.vhdl
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create a VHDL synthesis stub for the sub-design. This may lead to errors in top level synthesis of the design. Error reported: $_RESULT"
}

if { [catch {
  write_verilog -force -mode funcsim E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug_sim_netlist.v
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create the Verilog functional simulation sub-design file. Post-Synthesis Functional Simulation with this file may not be possible or may give incorrect results. Error reported: $_RESULT"
}

if { [catch {
  write_vhdl -force -mode funcsim E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug_sim_netlist.vhdl
} _RESULT ] } { 
  puts "CRITICAL WARNING: Unable to successfully create the VHDL functional simulation sub-design file. Post-Synthesis Functional Simulation with this file may not be possible or may give incorrect results. Error reported: $_RESULT"
}

add_files E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug_stub.v -of_objects [get_files E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/core_sources/chipscope_ila/xlx_ku_vivado_debug.xci]

add_files E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug_stub.vhdl -of_objects [get_files E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/core_sources/chipscope_ila/xlx_ku_vivado_debug.xci]

add_files E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug_sim_netlist.v -of_objects [get_files E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/core_sources/chipscope_ila/xlx_ku_vivado_debug.xci]

add_files E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug_sim_netlist.vhdl -of_objects [get_files E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/core_sources/chipscope_ila/xlx_ku_vivado_debug.xci]

add_files E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug.dcp -of_objects [get_files E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/core_sources/chipscope_ila/xlx_ku_vivado_debug.xci]

if {[file isdir E:/esperimenti/alice/zdc/gbt-fpga/trunk/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.ip_user_files/ip/xlx_ku_vivado_debug]} {
  catch { 
    file copy -force E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug_sim_netlist.v E:/esperimenti/alice/zdc/gbt-fpga/trunk/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.ip_user_files/ip/xlx_ku_vivado_debug
  }
}

if {[file isdir E:/esperimenti/alice/zdc/gbt-fpga/trunk/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.ip_user_files/ip/xlx_ku_vivado_debug]} {
  catch { 
    file copy -force E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug_sim_netlist.vhdl E:/esperimenti/alice/zdc/gbt-fpga/trunk/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.ip_user_files/ip/xlx_ku_vivado_debug
  }
}

if {[file isdir E:/esperimenti/alice/zdc/gbt-fpga/trunk/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.ip_user_files/ip/xlx_ku_vivado_debug]} {
  catch { 
    file copy -force E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug_stub.v E:/esperimenti/alice/zdc/gbt-fpga/trunk/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.ip_user_files/ip/xlx_ku_vivado_debug
  }
}

if {[file isdir E:/esperimenti/alice/zdc/gbt-fpga/trunk/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.ip_user_files/ip/xlx_ku_vivado_debug]} {
  catch { 
    file copy -force E:/esperimenti/alice/zdc/gbt-fpga/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.runs/xlx_ku_vivado_debug_synth_1/xlx_ku_vivado_debug_stub.vhdl E:/esperimenti/alice/zdc/gbt-fpga/trunk/example_designs/xilinx_kultrascale/kcu105/vivado_project/xlx_kcu105_gbt_exmpldsgn.ip_user_files/ip/xlx_ku_vivado_debug
  }
}
file delete __synthesis_is_running__
close [open __synthesis_is_complete__ w]
