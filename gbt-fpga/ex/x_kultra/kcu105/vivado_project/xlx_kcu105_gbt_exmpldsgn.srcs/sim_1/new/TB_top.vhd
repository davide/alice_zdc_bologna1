----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 31.03.2020 15:33:34
-- Design Name: 
-- Module Name: TB_top - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity TB_top is
--  Port ( );
end TB_top;

architecture behavioral of TB_top is

component top is
    port
    (
    reset:                      in  std_logic; 
    SYSCLK_P:                   in  std_logic;      -- 300 MHz
    SYSCLK_N:                   in  std_logic;
    USER_CLOCK_P:               in  std_logic;      -- 125 MHz
    USER_CLOCK_N:               in  std_logic;
    SMA_MGT_REFCLK_P:           in  std_logic;      -- 120 MHz
    SMA_MGT_REFCLK_N:           in  std_logic;
    SFP0_TX_P:                  out std_logic;
    SFP0_TX_N:                  out std_logic;
    SFP0_RX_P:                  in  std_logic;
    SFP0_RX_N:                  in  std_logic;     
    SFP1_TX_P:                  out std_logic;
    SFP1_TX_N:                  out std_logic;
    SFP1_RX_P:                  in  std_logic;
    SFP1_RX_N:                  in  std_logic;       
    SFP0_TX_DISABLE:            out std_logic;
    SFP1_TX_DISABLE:            out std_logic;
    USER_SMA_GPIO_P:            out std_logic;    
    USER_SMA_GPIO_N:            out std_logic
    );
end component top;

signal reset:               std_logic;
signal SYSCLK_P:            std_logic;
signal SYSCLK_N:            std_logic;
signal USER_CLOCK_P:        std_logic;
signal USER_CLOCK_N:        std_logic;
signal SMA_MGT_REFCLK_P:    std_logic;
signal SMA_MGT_REFCLK_N:    std_logic;
signal SFP0_TX_P:           std_logic;
signal SFP0_TX_N:           std_logic;
signal SFP0_RX_P:           std_logic;
signal SFP0_RX_N:           std_logic;
signal SFP1_TX_P:           std_logic;
signal SFP1_TX_N:           std_logic;
signal SFP1_RX_P:           std_logic;
signal SFP1_RX_N:           std_logic;
signal SFP0_TX_DISABLE:     std_logic;
signal SFP1_TX_DISABLE:     std_logic;
signal USER_SMA_GPIO_P:     std_logic;
signal USER_SMA_GPIO_N:     std_logic;

signal counter:             integer range 0 to 300000;

begin

top_instance: top
    port map
    (
    reset                       => reset,
    SYSCLK_P                    => SYSCLK_P,            -- 300 MHz
    SYSCLK_N                    => SYSCLK_N,
    USER_CLOCK_P                => USER_CLOCK_P,        -- 125 MHz
    USER_CLOCK_N                => USER_CLOCK_N,
    SMA_MGT_REFCLK_P            => SMA_MGT_REFCLK_P,    -- 120 MHz
    SMA_MGT_REFCLK_N            => SMA_MGT_REFCLK_N,
    SFP0_TX_P                   => SFP0_TX_P,
    SFP0_TX_N                   => SFP0_TX_N,
    SFP0_RX_P                   => SFP0_RX_P,
    SFP0_RX_N                   => SFP0_RX_N,    
    SFP1_TX_P                   => SFP1_TX_P,
    SFP1_TX_N                   => SFP1_TX_N,
    SFP1_RX_P                   => SFP1_RX_P,
    SFP1_RX_N                   => SFP1_RX_N,     
    SFP0_TX_DISABLE             => SFP0_TX_DISABLE,
    SFP1_TX_DISABLE             => SFP1_TX_DISABLE,
    USER_SMA_GPIO_P             => USER_SMA_GPIO_P,   
    USER_SMA_GPIO_N             => USER_SMA_GPIO_N
    );

-- 300 MHz
SYSCLK_proc: process
    begin
        loop
            SYSCLK_N <= '0', '1' after 1.66 ns;
            SYSCLK_P <= '1', '0' after 1.66 ns;
            wait for 3.33 ns;
        end loop;
        wait;
 end process SYSCLK_proc;
 
 -- 125 MHz
USER_CLOCK_proc: process
    begin
        loop
            USER_CLOCK_N <= '0', '1' after 4 ns;
            USER_CLOCK_P <= '1', '0' after 4 ns;
            wait for 8 ns;
        end loop;
        wait;
 end process USER_CLOCK_proc;
 
  -- 120 MHz
SMA_MGT_REFCLK_proc: process
    begin
        loop
            SMA_MGT_REFCLK_N <= '0', '1' after 4.16 ns;
            SMA_MGT_REFCLK_P <= '1', '0' after 4.16 ns;
            wait for 8.33 ns;
        end loop;
        wait;
 end process SMA_MGT_REFCLK_proc;
 
 process(USER_CLOCK_P)
 begin
    if(rising_edge(USER_CLOCK_P)) then
        counter <= counter + 1;
        if(counter < 10) then
            reset   <= '1';
        else
            reset   <= '0';
        end if;
    end if;
end process;
 
end behavioral;
