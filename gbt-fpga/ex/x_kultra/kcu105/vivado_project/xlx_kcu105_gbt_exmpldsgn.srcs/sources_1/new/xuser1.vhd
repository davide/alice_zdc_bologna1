----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.02.2020 12:22:42
-- Design Name: 
-- Module Name: event_generator - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity xuser1 is
    port
    (
    -----------------------------------------------------------------
    reset:                     in     std_logic;
	clock_240:     			   in     std_logic;
	-----------------------------------------------------------------
	signal_enable:             in     std_logic;
    general_reset:             in     std_logic;
	orbit_reset:               in     std_logic;
	orbit_valid:               in     std_logic;
	decoded_trigger:           in     std_logic_vector(79 downto 0);
	-----------------------------------------------------------------
	adc3_data_in:              in     std_logic_vector(23 downto 0);
    adc2_data_in:              in     std_logic_vector(23 downto 0);
    adc1_data_in:              in     std_logic_vector(23 downto 0);
    adc0_data_in:              in     std_logic_vector(23 downto 0); 
    -----------------------------------------------------------------
	bunch_dataword3:           out    std_logic_vector(81 downto 0);
	empty_bc_fifo3:            out    std_logic;
	readenable_control3:       in     std_logic;
	valid_bc_fifo3:            out    std_logic;
	bunch_dataword2:           out    std_logic_vector(81 downto 0);
	empty_bc_fifo2:            out    std_logic;
	readenable_control2:       in     std_logic;
	valid_bc_fifo2:            out    std_logic;
	bunch_dataword1:           out    std_logic_vector(81 downto 0);
	empty_bc_fifo1:            out    std_logic;
	readenable_control1:       in     std_logic;
	valid_bc_fifo1:            out    std_logic;
	bunch_dataword0:           out    std_logic_vector(81 downto 0);
	empty_bc_fifo0:            out    std_logic;
	readenable_control0:       in     std_logic;
	valid_bc_fifo0:            out    std_logic
	-----------------------------------------------------------------
    );		
end xuser1;

architecture behavioral of xuser1 is

component header is
port
    (
    -----------------------------------------------------------------
    reset:                     in     std_logic;
	clock_240:     			   in     std_logic;
	-----------------------------------------------------------------
	signal_enable:             in     std_logic;
    general_reset:             in     std_logic;
	orbit_reset:               in     std_logic;
	orbit_valid:               in     std_logic;
	decoded_trigger:           in     std_logic_vector(79 downto 0);
	-----------------------------------------------------------------
	adc_data_in:               in     std_logic_vector(23 downto 0);
	-----------------------------------------------------------------
	push_bc_fifo:              out    std_logic;
	bc_fifo_data:              out    std_logic_vector(81 downto 0)
	-----------------------------------------------------------------
	);
end component header;

component bunch_crossing_fifo
  port (
    clk : in std_logic;
    rst : in std_logic;
    din : in std_logic_vector(81 downto 0);
    wr_en : in std_logic;
    rd_en : in std_logic;
    dout : out std_logic_vector(81 downto 0);
    full : out std_logic;
    empty : out std_logic;
    valid : out std_logic
  );
end component bunch_crossing_fifo;

signal push_bc_fifo0:       std_logic;
signal bc_fifo_data0:       std_logic_vector(81 downto 0);
signal push_bc_fifo1:       std_logic;
signal bc_fifo_data1:       std_logic_vector(81 downto 0);
signal push_bc_fifo2:       std_logic;
signal bc_fifo_data2:       std_logic_vector(81 downto 0);
signal push_bc_fifo3:       std_logic;
signal bc_fifo_data3:       std_logic_vector(81 downto 0);

signal full_bc_fifo0:       std_logic;
signal full_bc_fifo1:       std_logic;
signal full_bc_fifo2:       std_logic;
signal full_bc_fifo3:       std_logic;
   
begin

---------------------------------------------------------------------
header_instance0: header
port map
    (
    -----------------------------------------------------------------
    reset                      => reset,
	clock_240                  => clock_240,
	-----------------------------------------------------------------
	signal_enable              => signal_enable,
    general_reset              => general_reset,
	orbit_reset                => orbit_reset,
	orbit_valid                => orbit_valid,
	decoded_trigger            => decoded_trigger,
	-----------------------------------------------------------------
	adc_data_in                => adc0_data_in,
	-----------------------------------------------------------------
	push_bc_fifo               => push_bc_fifo0,
	bc_fifo_data               => bc_fifo_data0
	-----------------------------------------------------------------
	);

bunch_crossing_fifo_instance0: bunch_crossing_fifo
  port map(
    clk             => clock_240,
    rst             => reset,
    din             => bc_fifo_data0,
    wr_en           => push_bc_fifo0,
    rd_en           => readenable_control0,
    dout            => bunch_dataword0,
    full            => full_bc_fifo0,
    empty           => empty_bc_fifo0,
    valid           => valid_bc_fifo0
  );
---------------------------------------------------------------------

---------------------------------------------------------------------  	
header_instance1: header
port map
    (
    -----------------------------------------------------------------
    reset                      => reset,
	clock_240                  => clock_240,
	-----------------------------------------------------------------
	signal_enable              => signal_enable,
    general_reset              => general_reset,
	orbit_reset                => orbit_reset,
	orbit_valid                => orbit_valid,
	decoded_trigger            => decoded_trigger,
	-----------------------------------------------------------------
	adc_data_in                => adc1_data_in,
	-----------------------------------------------------------------
	push_bc_fifo               => push_bc_fifo1,
	bc_fifo_data               => bc_fifo_data1
	-----------------------------------------------------------------
	);
	
bunch_crossing_fifo_instance1: bunch_crossing_fifo
  port map(
    clk             => clock_240,
    rst             => reset,
    din             => bc_fifo_data1,
    wr_en           => push_bc_fifo1,
    rd_en           => readenable_control1,
    dout            => bunch_dataword1,
    full            => full_bc_fifo1,
    empty           => empty_bc_fifo1,
    valid           => valid_bc_fifo1
  );
---------------------------------------------------------------------

---------------------------------------------------------------------	
header_instance2: header
port map
    (
    -----------------------------------------------------------------
    reset                      => reset,
	clock_240                  => clock_240,
	-----------------------------------------------------------------
	signal_enable              => signal_enable,
    general_reset              => general_reset,
	orbit_reset                => orbit_reset,
	orbit_valid                => orbit_valid,
	decoded_trigger            => decoded_trigger,
	-----------------------------------------------------------------
	adc_data_in                => adc2_data_in,
	-----------------------------------------------------------------
	push_bc_fifo               => push_bc_fifo2,
	bc_fifo_data               => bc_fifo_data2
	-----------------------------------------------------------------
	);

bunch_crossing_fifo_instance2: bunch_crossing_fifo
  port map(
    clk             => clock_240,
    rst             => reset,
    din             => bc_fifo_data2,
    wr_en           => push_bc_fifo2,
    rd_en           => readenable_control2,
    dout            => bunch_dataword2,
    full            => full_bc_fifo2,
    empty           => empty_bc_fifo2,
    valid           => valid_bc_fifo2
  );
---------------------------------------------------------------------

---------------------------------------------------------------------	
header_instance3: header
port map
    (
    -----------------------------------------------------------------
    reset                      => reset,
	clock_240                  => clock_240,
	-----------------------------------------------------------------
	signal_enable              => signal_enable,
    general_reset              => general_reset,
	orbit_reset                => orbit_reset,
	orbit_valid                => orbit_valid,
	decoded_trigger            => decoded_trigger,
	-----------------------------------------------------------------
	adc_data_in                => adc3_data_in,
	-----------------------------------------------------------------
	push_bc_fifo               => push_bc_fifo3,
	bc_fifo_data               => bc_fifo_data3
	-----------------------------------------------------------------
	);
	
bunch_crossing_fifo_instance3: bunch_crossing_fifo
  port map(
    clk             => clock_240,
    rst             => reset,
    din             => bc_fifo_data3,
    wr_en           => push_bc_fifo3,
    rd_en           => readenable_control3,
    dout            => bunch_dataword3,
    full            => full_bc_fifo3,
    empty           => empty_bc_fifo3,
    valid           => valid_bc_fifo3
  );
---------------------------------------------------------------------
end behavioral;
