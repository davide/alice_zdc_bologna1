----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 04.04.2020 12:30:33
-- Design Name: 
-- Module Name: multiplexer - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library work;
use work.top_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity multiplexer is
port
	(
	reset:             		   in  	  std_logic;
	clock_240:     			   in	  std_logic;         -- 240 MHz
	regs: 					   in     REGS_RECORD;
	----------------------------------------------------------------
	bunch_dataword:            in     std_logic_vector(81 downto 0);
	empty_bc_fifo:             in     std_logic;
	readenable_control:        out    std_logic;
	valid_bc_fifo:             in     std_logic;
	----------------------------------------------------------------
	data_to_gbt_sfp:           out    std_logic_vector(83 downto 0);
    data_valid_to_gbt_sfp:     out    std_logic
    );	
end multiplexer;

architecture behavioral of multiplexer is

    -- RDH signals --------------------------------------------------------------------------------------------------------------------------
	constant c_IDLE: 							std_logic_vector(3 downto 0) := x"0";  
	constant c_SOP: 							std_logic_vector(3 downto 0) := x"1"; 
	constant c_EOP: 							std_logic_vector(3 downto 0) := x"2"; 
	
	type   PSTATE is (WAIT_STATE, REC_TRIGGER_STATE, REC_TRIGGER_STATE1, 
					  HB_OPEN_SOP, HB_OPEN_RDH0, HB_OPEN_RDH1, HB_OPEN_RDH2, HB_OPEN_RDH3, 
					  HB_CLOSE_EOP, HB_CLOSE_SOP, HB_CLOSE_RDH0, HB_CLOSE_RDH1, HB_CLOSE_RDH2, HB_CLOSE_RDH3, HB_CLOSE_EOP1,
					  DATA_STATE, PHY_EOP, PHY_SOP, PHY_RDH0, PHY_RDH1, PHY_RDH2, PHY_RDH3);
	attribute syn_encoding: 					string;
    attribute syn_encoding of PSTATE: type is "onehot";
    signal state_pattern: 						PSTATE;
	
	signal rdh_header_version:					std_logic_vector(7 downto 0);
	signal rdh_header_size:						std_logic_vector(7 downto 0);
	signal rdh_block_length:					std_logic_vector(15 downto 0);
	signal rdh_fee_id:							std_logic_vector(15 downto 0);
	signal rdh_priority_bit:					std_logic_vector(7 downto 0);
	signal rdh_hb_orbit:						std_logic_vector(31 downto 0);
	signal rdh_hb_bc:							std_logic_vector(11 downto 0);
	signal rdh_trg_type:						std_logic_vector(31 downto 0);
	signal rdh_detector_field:					std_logic_vector(31 downto 0);
	signal rdh_par:								std_logic_vector(15 downto 0);
	signal rdh_stop_bit:						std_logic_vector(7 downto 0);
	signal rdh_pages_counter:					std_logic_vector(15 downto 0);
	signal pages_counter:						std_logic_vector(15 downto 0);
	
	signal rdh_word0:							std_logic_vector(79 downto 0);
	signal rdh_word1:							std_logic_vector(79 downto 0);
	signal rdh_word2:							std_logic_vector(79 downto 0);
	signal rdh_word3:							std_logic_vector(79 downto 0);
	signal rdh_word0_old:						std_logic_vector(79 downto 0);
	signal rdh_word1_old:						std_logic_vector(79 downto 0);
	signal rdh_word2_old:						std_logic_vector(79 downto 0);
	signal rdh_word3_old:						std_logic_vector(79 downto 0);
	
	signal count_data:							integer range 0 to 1023;
	signal eot_received:						std_logic_vector(1 downto 0);
	signal hb_and_phy:							std_logic;
	
	signal readenable_control_int:            std_logic;

begin

readenable_control    <= readenable_control_int;

--  -----------------------------------------------------------------------------------------------------------------------------------------
--	-- GBTx packet header: RDH --------------------------------------------------------------------------------------------------------------
--	-----------------------------------------------------------------------------------------------------------------------------------------

	-- RDH word 0
	rdh_header_version 	<= x"05";
	rdh_header_size 	<= x"40";		
	rdh_block_length	<= CONV_STD_LOGIC_VECTOR(RDH_SIZE, rdh_block_length'length);		
	rdh_fee_id			<= regs.pcbid(15 downto 0);
	rdh_priority_bit	<= x"00";
	
	-- RDH word 1	
	rdh_hb_orbit		<= bunch_dataword(79 downto 48) when (bunch_dataword(HB_TT) = '1' and bunch_dataword(1 downto 0) = "00") else (others => '0');     -- HB
	
	-- RDH word 2
	rdh_hb_bc			<= bunch_dataword(43 downto 32) when (bunch_dataword(HB_TT) = '1' and bunch_dataword(1 downto 0) = "00") else (others => '0');	 -- HB
	rdh_trg_type		<= bunch_dataword(31 downto 0) when (bunch_dataword(HB_TT) = '1' and bunch_dataword(1 downto 0) = "00") else (others => '0');	 -- HB;
	
	-- RDH word 3
	rdh_detector_field	<= x"0000070F";
	rdh_par				<= x"0000";
	rdh_stop_bit		<= "00000000";
	rdh_pages_counter	<= x"0000";
		
	rdh_word0 			<= x"0000000000" & rdh_priority_bit & rdh_fee_id & rdh_header_size & rdh_header_version;	-- 40 zeros | 8 | 16 | 8 | 8						   																	   
	rdh_word1			<= x"0000" & rdh_hb_orbit & x"00000" & rdh_hb_bc;										    -- 16 zeros | 32 | 20 | 12								   																											   
	rdh_word2			<= x"000000" & rdh_stop_bit & rdh_pages_counter & rdh_trg_type;						        -- 24 zeros | 8 | 16 | 32			   																							   
	rdh_word3			<= x"00000000" & rdh_par & rdh_detector_field;	                                            -- 24 zeros | 16 | 8 | 16 | 32
						   																							
--	-----------------------------------------------------------------------------------------------------------------------------------------
	
--	-----------------------------------------------------------------------------------------------------------------------------------------
--	-- towards GBTx	-------------------------------------------------------------------------------------------------------------------------
--	-----------------------------------------------------------------------------------------------------------------------------------------

	state_machine_process: process(reset, clock_240)
	begin
		if(reset = '1') then
			state_pattern				<= WAIT_STATE;
			count_data					<= 0;
			pages_counter				<= (others => '0');
			eot_received				<= "00";
			hb_and_phy					<= '0';
			data_to_gbt_sfp             <= (others => '0');
			data_valid_to_gbt_sfp	    <= '0';
		elsif(rising_edge(clock_240)) then
			if(state_pattern = WAIT_STATE) then
				if(empty_bc_fifo = '0') then													
					readenable_control_int	<= '1';
					state_pattern		    <= REC_TRIGGER_STATE;
				else
					state_pattern		    <= WAIT_STATE;
				end if;
				eot_received			<= "00";
				hb_and_phy				<= '0';
			elsif(state_pattern = REC_TRIGGER_STATE) then
				readenable_control_int    <= '0';
				state_pattern		    <= REC_TRIGGER_STATE1;
			elsif(state_pattern = REC_TRIGGER_STATE1) then
				readenable_control_int		<= '0';
				if(bunch_dataword(HB_TT) = '1') then
					if(bunch_dataword(SOC_TT) = '1') then				-- after a HB+SOC
						state_pattern		<= HB_OPEN_SOP;
					elsif(bunch_dataword(EOC_TT) = '1') then			-- after a HB+EOC
						eot_received		<= "01";
						state_pattern		<= HB_CLOSE_EOP;
					elsif(bunch_dataword(PHYSICS_TT) = '1') then	    -- after a HB and TOF trigger in the same BC
						state_pattern		<= HB_CLOSE_EOP;
						hb_and_phy			<= '1';
					else												-- after a HB
						state_pattern		<= HB_CLOSE_EOP;
					end if;
				else													-- after a TOF trigger (l1msg_dto_gbtck(TOF_TT) = '1'or l1msg_dto_gbtck(PHYSICS_TT))
					if(empty_bc_fifo = '0') then
						readenable_control_int	<= '1';
					else
						readenable_control_int	<= '0';
					end if;
					state_pattern	<= DATA_STATE;
				end if;
				data_to_gbt_sfp             <= (others => '0');
			    data_valid_to_gbt_sfp	    <= '0';
			-- HB_OPEN ----------------------------------------------------------------------------------------------------------------------------------------
			elsif(state_pattern = HB_OPEN_SOP) then
				data_to_gbt_sfp	<= x"0" & c_SOP & x"0000000000000000000";
				data_valid_to_gbt_sfp	<= '0';
				readenable_control_int		<= '0';
				state_pattern		<= HB_OPEN_RDH0;
				pages_counter		<= (others => '0');
				count_data			<= 0;
			elsif(state_pattern = HB_OPEN_RDH0) then
				data_to_gbt_sfp	<= x"0" & rdh_word0;
				data_valid_to_gbt_sfp	<= '1';
				rdh_word0_old		<= rdh_word0;
				state_pattern		<= HB_OPEN_RDH1;
			elsif(state_pattern = HB_OPEN_RDH1) then
				data_to_gbt_sfp	<= x"0" & rdh_word1;
				data_valid_to_gbt_sfp	<= '1';
				rdh_word1_old		<= rdh_word1;
				state_pattern		<= HB_OPEN_RDH2;
			elsif(state_pattern = HB_OPEN_RDH2) then
				data_to_gbt_sfp	<= x"0" & rdh_word2;
				data_valid_to_gbt_sfp	<= '1';
				rdh_word2_old		<= rdh_word2;
				state_pattern		<= HB_OPEN_RDH3;
			elsif(state_pattern = HB_OPEN_RDH3) then
				data_to_gbt_sfp	<= x"0" & rdh_word3;
				data_valid_to_gbt_sfp	<= '1';			
				rdh_word3_old		<= rdh_word3;
				if(hb_and_phy = '1') then
					state_pattern	<= DATA_STATE;
				elsif(eot_received = "01") then
					state_pattern 	<= HB_CLOSE_EOP;
					eot_received	<= "10";
				else
					state_pattern	<= WAIT_STATE;
				end if;
			-- HB_CLOSE ---------------------------------------------------------------------------------------------------------------------------------------
			elsif(state_pattern = HB_CLOSE_EOP) then
				data_to_gbt_sfp	<= x"0" & c_EOP & x"0000000000000000000";
				data_valid_to_gbt_sfp	<= '0';			
				pages_counter		<= pages_counter + 1;
				count_data			<= 0;
				state_pattern		<= HB_CLOSE_SOP;
			elsif(state_pattern = HB_CLOSE_SOP) then
				data_to_gbt_sfp	<= x"0" & c_SOP & x"0000000000000000000";
				data_valid_to_gbt_sfp	<= '0';
				state_pattern		<= HB_CLOSE_RDH0;
			elsif(state_pattern = HB_CLOSE_RDH0) then
				data_to_gbt_sfp	<= x"0" & rdh_word0_old;
				data_valid_to_gbt_sfp	<= '1';
				state_pattern		<= HB_CLOSE_RDH1;
			elsif(state_pattern = HB_CLOSE_RDH1) then
				data_to_gbt_sfp	<= x"0" & rdh_word1_old;
				data_valid_to_gbt_sfp	<= '1';
				state_pattern		<= HB_CLOSE_RDH2;
			elsif(state_pattern = HB_CLOSE_RDH2) then
				data_to_gbt_sfp	<= x"0" & rdh_word2_old(79 downto 56) & "00000001" & pages_counter & rdh_word2_old(31 downto 0);	-- RDH v5
				data_valid_to_gbt_sfp	<= '1';
				state_pattern		<= HB_CLOSE_RDH3;
			elsif(state_pattern = HB_CLOSE_RDH3) then	
				data_to_gbt_sfp	<= x"0" & rdh_word3_old(79 downto 32) & x"00" & x"00" & rdh_word3_old(15 downto 0);			-- RDH v5
				data_valid_to_gbt_sfp	<= '1';			
				state_pattern		<= HB_CLOSE_EOP1;
			elsif(state_pattern = HB_CLOSE_EOP1) then
				data_to_gbt_sfp	<= x"0" & c_EOP & x"0000000000000000000";
				data_valid_to_gbt_sfp	<= '0';			
				if(eot_received <= "01") then
					state_pattern 	<= HB_OPEN_SOP;
				else
					state_pattern	<= WAIT_STATE;
					eot_received	<= "00";
				end if;
			-- DATA transmission ------------------------------------------------------------------------------------------------------------------------------
			elsif(state_pattern = DATA_STATE) then
				if(valid_bc_fifo = '1') then
					data_to_gbt_sfp	      <= "00" & bunch_dataword;
					data_valid_to_gbt_sfp	  <= '1';
				else
					data_to_gbt_sfp	      <= x"00000000000000000000" & c_IDLE;
					data_valid_to_gbt_sfp	  <= '0';
				end if;
				if(readenable_control_int = '1') then
					count_data			<= count_data + 1;
				end if;				
				if(count_data = 507 and readenable_control_int = '1') then		
					state_pattern			<= DATA_STATE;
					readenable_control_int	<= '0';
				elsif(count_data > 507) then		
					state_pattern			<= PHY_EOP;
					readenable_control_int	<= '0';
				else
					state_pattern			<= DATA_STATE;
					readenable_control_int	<= not empty_bc_fifo;
				end if;
			elsif(state_pattern = PHY_EOP) then
				data_to_gbt_sfp	<= x"0" & c_EOP & x"0000000000000000000";
				data_valid_to_gbt_sfp	<= '0';			
				state_pattern		<= PHY_SOP;
			elsif(state_pattern = PHY_SOP) then
				data_to_gbt_sfp	<= x"0" & c_SOP & x"0000000000000000000";
				data_valid_to_gbt_sfp	<= '0';
				state_pattern		<= PHY_RDH0;
				pages_counter		<= pages_counter + 1;
			elsif(state_pattern = PHY_RDH0) then
				data_to_gbt_sfp	<= x"0" & rdh_word0_old;
				data_valid_to_gbt_sfp	<= '1';
				state_pattern		<= PHY_RDH1;
			elsif(state_pattern = PHY_RDH1) then
				data_to_gbt_sfp	<= x"0" & rdh_word1_old;
				data_valid_to_gbt_sfp	<= '1';
				state_pattern		<= PHY_RDH2;
			elsif(state_pattern = PHY_RDH2) then
				data_to_gbt_sfp	<= x"0" & rdh_word2_old(79 downto 48) & pages_counter & rdh_word2_old(31 downto 0);		-- RDH v5
				data_valid_to_gbt_sfp	<= '1';
				state_pattern		<= PHY_RDH3;
			elsif(state_pattern = PHY_RDH3) then
			    data_to_gbt_sfp	<= x"0" & rdh_word3_old;																-- RDH v5
				data_valid_to_gbt_sfp	<= '1';			
				count_data			<= 0;
				state_pattern		<= DATA_STATE;
			end if;
		end if;
	end process state_machine_process;
		
	-----------------------------------------------------------------------------------------------------------------------------------------


end behavioral;
