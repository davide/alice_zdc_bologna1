----------------------------------------------------------------------------------
-- Company: INFN Bologna
-- Engineer: Davide Falchieri
-- 
-- Create Date: 25.02.2020 11:07:07
-- Design Name: 
-- Module Name: gbtfpga - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_exampledesign_package.all;

entity gbtfpga is
    port
    (
    reset:                      in  std_logic; 
    sysclk:                     in  std_logic;  -- 300 MHz
    user_clock:                 in  std_logic;  -- 125 MHz
    SMA_MGT_REFCLK_P:           in  std_logic;  -- 120 MHz
    SMA_MGT_REFCLK_N:           in  std_logic;
    SFP0_TX_P:                  out std_logic;
    SFP0_TX_N:                  out std_logic;
    SFP0_RX_P:                  in  std_logic;
    SFP0_RX_N:                  in  std_logic;     
    SFP1_TX_P:                  out std_logic;
    SFP1_TX_N:                  out std_logic;
    SFP1_RX_P:                  in  std_logic;
    SFP1_RX_N:                  in  std_logic;       
    SFP0_TX_DISABLE:            out std_logic;
    SFP1_TX_DISABLE:            out std_logic;
    ----------------------------------------------------------
    data_to_gbt_sfp0:           in  std_logic_vector(83 downto 0);
    data_valid_to_gbt_sfp0:     in  std_logic;
    data_from_gbt_sfp0:         out std_logic_vector(83 downto 0);                             
    data_valid_from_gbt_sfp0:   out std_logic;                        
    clock_gbt_rx_sfp0:          out std_logic;
  
    ----------------------------------------------------------
    data_to_gbt_sfp1:           in  std_logic_vector(83 downto 0);
    data_valid_to_gbt_sfp1:     in  std_logic;
    data_from_gbt_sfp1:         out std_logic_vector(83 downto 0);                             
    data_valid_from_gbt_sfp1:   out std_logic;                        
    clock_gbt_rx_sfp1:          out std_logic;
    ----------------------------------------------------------
    USER_SMA_GPIO_P:            out std_logic;    
    USER_SMA_GPIO_N:            out std_logic
    );
end gbtfpga;

architecture behavioral of gbtfpga is

component kcu105_gbt_example_design
    port
    (
      --===============--     
      -- General reset --     
      --===============--     

      CPU_RESET                                      : in  std_logic;     
      
      --===============--
      -- Clocks scheme --
      --===============-- 
      
      -- System clock:
      ----------------
      sysclk                                       : in  std_logic;
            
      -- Fabric clock:
      ----------------     

      user_clock                                     : in  std_logic;
      
      -- MGT(GTX) reference clock:
      ----------------------------
      
      -- Comment: * The MGT reference clock MUST be provided by an external clock generator.
      --
      --          * The MGT reference clock frequency must be 120MHz for the latency-optimized GBT Bank.      
      
      SMA_MGT_REFCLK_P                               : in  std_logic;
      SMA_MGT_REFCLK_N                               : in  std_logic; 
      
      --==========--
      -- MGT(GTX) --
      --==========--                   
      
      -- Serial lanes:
      ----------------
      
      SFP0_TX_P                                      : out std_logic;
      SFP0_TX_N                                      : out std_logic;
      SFP0_RX_P                                      : in  std_logic;
      SFP0_RX_N                                      : in  std_logic;
      
      SFP1_TX_P                                      : out std_logic;
      SFP1_TX_N                                      : out std_logic;
      SFP1_RX_P                                      : in  std_logic;
      SFP1_RX_N                                      : in  std_logic;    
      
      -- SFP control:
      ---------------
      
      SFP0_TX_DISABLE                                : out std_logic;    
      SFP1_TX_DISABLE                                : out std_logic;
      
      -- GBT I/O signals:
      -------------------
      data_to_gbt_sfp0                               : in  std_logic_vector(83 downto 0);
      data_valid_to_gbt_sfp0                         : in  std_logic;
      
      data_from_gbt_sfp0                             : out std_logic_vector(83 downto 0);                             
      data_valid_from_gbt_sfp0                       : out std_logic;                        
      clock_gbt_rx_sfp0                              : out std_logic;
      
      data_to_gbt_sfp1                               : in  std_logic_vector(83 downto 0);
      data_valid_to_gbt_sfp1                         : in  std_logic;
      
      data_from_gbt_sfp1                             : out std_logic_vector(83 downto 0);                             
      data_valid_from_gbt_sfp1                       : out std_logic;                        
      clock_gbt_rx_sfp1                              : out std_logic;
      
      --====================--
      -- Signals forwarding --
      --====================--
      
      -- SMA output:
      --------------
      USER_SMA_GPIO_P                                : out std_logic;    
      USER_SMA_GPIO_N                                : out std_logic
    );
 end component kcu105_gbt_example_design;

begin

kcu105_gbt_example_design_instance: kcu105_gbt_example_design
    port map
    (  
      CPU_RESET                                      => reset, 
      sysclk                                         => sysclk,
      user_clock                                     => user_clock,
      SMA_MGT_REFCLK_P                               => SMA_MGT_REFCLK_P,
      SMA_MGT_REFCLK_N                               => SMA_MGT_REFCLK_N,  
      SFP0_TX_P                                      => SFP0_TX_P,
      SFP0_TX_N                                      => SFP0_TX_N,
      SFP0_RX_P                                      => SFP0_RX_P,
      SFP0_RX_N                                      => SFP0_RX_N,   
      SFP1_TX_P                                      => SFP1_TX_P,
      SFP1_TX_N                                      => SFP1_TX_N,
      SFP1_RX_P                                      => SFP1_RX_P,
      SFP1_RX_N                                      => SFP1_RX_N,     
      SFP0_TX_DISABLE                                => SFP0_TX_DISABLE,  
      SFP1_TX_DISABLE                                => SFP1_TX_DISABLE,
      -------------------------------------------------------------------------
      data_to_gbt_sfp0                               => data_to_gbt_sfp0,
      data_valid_to_gbt_sfp0                         => data_valid_to_gbt_sfp0,
      data_from_gbt_sfp0                             => data_from_gbt_sfp0,
      data_valid_from_gbt_sfp0                       => data_valid_from_gbt_sfp0,
      clock_gbt_rx_sfp0                              => clock_gbt_rx_sfp0,
      -------------------------------------------------------------------------
      data_to_gbt_sfp1                               => data_to_gbt_sfp1,
      data_valid_to_gbt_sfp1                         => data_valid_to_gbt_sfp1,
      data_from_gbt_sfp1                             => data_from_gbt_sfp1,
      data_valid_from_gbt_sfp1                       => data_valid_from_gbt_sfp1,
      clock_gbt_rx_sfp1                              => clock_gbt_rx_sfp1,
      -------------------------------------------------------------------------
      USER_SMA_GPIO_P                                => USER_SMA_GPIO_P,
      USER_SMA_GPIO_N                                => USER_SMA_GPIO_N
    );  
    
end behavioral;
