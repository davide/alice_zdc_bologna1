----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 19.03.2020 16:24:40
-- Design Name: 
-- Module Name: fmc1 - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity fmc1 is
port
    (
    clock_240:      in      std_logic;                          -- 240 MHz clock
    reset:          in      std_logic;
    adc3_data_out:  out     std_logic_vector(23 downto 0);
    adc2_data_out:  out     std_logic_vector(23 downto 0);
    adc1_data_out:  out     std_logic_vector(23 downto 0);
    adc0_data_out:  out     std_logic_vector(23 downto 0)   
    );
end fmc1;

architecture behavioral of fmc1 is

component adc is
port
    (
    clock_240:      in      std_logic;                          -- 240 MHz clock
    reset:          in      std_logic;
    adc_data_out:   out     std_logic_vector(23 downto 0)
    );
end component adc;

begin

adc_instance0: adc
port map
    (
    clock_240       => clock_240,
    reset           => reset,
    adc_data_out    => adc0_data_out
    );

adc_instance1: adc
port map
    (
    clock_240       => clock_240,
    reset           => reset,
    adc_data_out    => adc1_data_out
    );

adc_instance2: adc
port map
    (
    clock_240       => clock_240,
    reset           => reset,
    adc_data_out    => adc2_data_out
    );
 
adc_instance3: adc
port map
    (
    clock_240       => clock_240,
    reset           => reset,
    adc_data_out    => adc3_data_out
    );   
  
end behavioral;
