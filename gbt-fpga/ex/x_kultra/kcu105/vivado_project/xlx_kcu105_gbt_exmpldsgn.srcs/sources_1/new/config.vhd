----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.02.2020 16:46:14
-- Design Name: 
-- Module Name: swt_interface - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library work;
use work.top_pkg.all;

entity config is
    port
    (
    reset:             		     in  	std_logic;
	clock_240:     			     in  	std_logic;
	regs: 					     inout  REGS_RECORD;
	-- GBT FPGA interface
	------------------------------------------------------------------	
    data_to_gbt_sfp1:            out    std_logic_vector(83 downto 0);
    data_valid_to_gbt_sfp1:      out    std_logic;
    data_from_gbt_sfp1:          in     std_logic_vector(83 downto 0);                             
    data_valid_from_gbt_sfp1:    in     std_logic
    ------------------------------------------------------------------	
    );
end config;

architecture behavioral of config is

signal swt_address:     std_logic_vector(31 downto 0);
signal swt_data_in:     std_logic_vector(31 downto 0);
signal swt_command:     std_logic;
signal swt_valid_in:    std_logic;

signal swt_data_out:    std_logic_vector(31 downto 0);
signal swt_valid_out:   std_logic;

constant c_IDLE: 		std_logic_vector(3 downto 0) := x"0";  
constant c_SOP: 		std_logic_vector(3 downto 0) := x"1"; 
constant c_EOP: 		std_logic_vector(3 downto 0) := x"2";
constant c_SWT: 		std_logic_vector(3 downto 0) := x"3"; 

begin

    process(clock_240, reset)    
    begin
        if(reset = '1') then		
            swt_address     <= (others => '0');
            swt_data_in     <= (others => '0');
            swt_command     <= '0';
            swt_valid_in    <= '0';
        elsif(rising_edge(clock_240)) then
            if(data_valid_from_gbt_sfp1 = '1' and data_from_gbt_sfp1(79 downto 76) = c_SWT) then     -- swt command
                swt_address     <= data_from_gbt_sfp1(31 downto 0);     -- swt address
                swt_data_in     <= data_from_gbt_sfp1(63 downto 32);    -- swt data in
                swt_command     <= data_from_gbt_sfp1(64);              -- swt command read or write
                swt_valid_in    <= data_from_gbt_sfp1(65);              -- swt command valid
            end if;     
        end if;
    end process;
    
    process(clock_240, reset)    
    begin
        if(reset = '1') then
            swt_data_out    <= (others => '0');
            swt_valid_out   <= '0';
        elsif(rising_edge(clock_240)) then
            if(swt_valid_in = '1') then
                if(swt_command = '1') then      -- READ command
                    case swt_address is
                        when A_STATUS       =>   swt_data_out   <= regs.status;
                        when A_CTRL         =>   swt_data_out   <= regs.ctrl;
                        when A_FWREV        =>   swt_data_out   <= regs.fwrev;
                        when A_PCBID        =>   swt_data_out   <= regs.pcbid;
                        when A_BERTEST      =>   swt_data_out   <= regs.ber_test;
                        
                        when others         =>   swt_data_out   <= FILLER;
                    end case;
                    swt_valid_out   <= '1';
                 else                           -- WRITE command
                    case swt_address is
                        when A_STATUS       =>   regs.status    <= swt_data_in;
                        when A_CTRL         =>   regs.ctrl      <= swt_data_in;
                        when A_FWREV        =>   regs.fwrev     <= swt_data_in;
                        when A_PCBID        =>   regs.pcbid     <= swt_data_in;
                        when A_BERTEST      =>   regs.ber_test  <= swt_data_in;
                        
                        when others         =>   null;
                    end case;
                    swt_data_out    <= swt_data_in; 
                    swt_valid_out   <= '1';
                 end if;
            else
                swt_valid_out   <= '0';
            end if;
        end if;
    end process;
    
    process(clock_240, reset)    
    begin
        if(reset = '1') then
            data_to_gbt_sfp1        <= (others => '0');
            data_valid_to_gbt_sfp1  <= '0';
        elsif(rising_edge(clock_240)) then
            if(swt_valid_out = '1') then
                data_to_gbt_sfp1        <= x"03" & x"00000000000" & swt_data_out;
                data_valid_to_gbt_sfp1  <= '1';
            else
                data_valid_to_gbt_sfp1  <= '0';
            end if;
        end if;
    end process;
    
end behavioral;
