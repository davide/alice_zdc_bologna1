----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.02.2020 17:13:30
-- Design Name: 
-- Module Name: top_pkg - package
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

package top_pkg is

  -------------------------------------------------------------------------------------------------------------------------------------------
  -- Register definition  -------------------------------------------------------------------------------------------------------------------
  -------------------------------------------------------------------------------------------------------------------------------------------
  subtype REGISTER_TYPE          is std_logic_vector(31 downto 0); 
  
  type REGS_RECORD is record  	
    status: 			REGISTER_TYPE;
    ctrl:               REGISTER_TYPE;
    fwrev:              REGISTER_TYPE;
    pcbid:              REGISTER_TYPE;
    ber_test:           REGISTER_TYPE;
  end record;

  constant RDH_SIZE:				integer := 4;	

  -------------------------------------------------------------------------------------------------------------------------------------------
  -- trigger types TT -----------------------------------------------------------------------------------------------------------------------
  -------------------------------------------------------------------------------------------------------------------------------------------
  constant ORBIT_TT:			integer :=  0;		-- orbit
  constant HB_TT: 				integer :=  1;		-- HB 
  constant HBr_TT: 				integer :=  2;		-- HB reject
  constant HBc_TT: 				integer :=  3;		-- Health check
  constant PHYSICS_TT: 			integer :=  4;		-- Physics trigger
  constant PREPULSE_TT: 		integer :=  5;		-- Prepulse
  constant CALIBRATION_TT:		integer :=  6;		-- Calibration
  constant SOT_TT: 				integer :=  7;		-- Start of Triggered Data
  constant EOT_TT: 				integer :=  8;		-- End of Triggered Data
  constant SOC_TT: 				integer :=  9;		-- Start of Continuous Data
  constant EOC_TT: 				integer :=  10;		-- End of Continuous Data
  constant TF_TT: 				integer :=  11;		-- Time Frame delimiter
  constant SPARE_TT:			integer :=  12;		-- Not used
  constant TOF_TT:				integer :=  31;		-- TOF special trigger  
  -------------------------------------------------------------------------------------------------------------------------------------------
  
  constant A_STATUS: 			std_logic_vector(31 downto 0) := x"00000000";  -- RW   - status register
  constant A_CTRL:  			std_logic_vector(31 downto 0) := x"00000001";  -- RW   - control register
  constant A_FWREV:             std_logic_vector(31 downto 0) := x"00000002";  -- RW   - firmware version register
  constant A_PCBID:             std_logic_vector(31 downto 0) := x"00000003";  -- RW   - board ID
  constant A_BERTEST:           std_logic_vector(31 downto 0) := x"00000004";  -- RW   - BER test register
  
  constant FILLER: 				std_logic_vector(31 downto 0) := X"10101010";
  
  -------------------------------------------------------------------------------------------------------------------------------------------
  -- ber_test -------------------------------------------------------------------------------------------------------------------------------
  -------------------------------------------------------------------------------------------------------------------------------------------
  constant BER_LOOP_EN:			integer :=  0;		-- enable GBTx loopback

end package;
