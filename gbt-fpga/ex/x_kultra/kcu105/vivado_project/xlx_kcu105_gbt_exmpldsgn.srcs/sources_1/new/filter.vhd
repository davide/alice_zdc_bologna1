----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 06.04.2020 18:51:03
-- Design Name: 
-- Module Name: filter - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library work;
use work.top_pkg.all;

entity filter is
port
	(
	reset:             		   in  	  std_logic;
	clock_240:     			   in	  std_logic;         -- 240 MHz
	regs: 					   in     REGS_RECORD;
	----------------------------------------------------------------
	bunch_dataword_a:          in     std_logic_vector(81 downto 0);
	empty_bc_fifo_a:           in     std_logic;
	readenable_control_a:      out    std_logic;
	valid_bc_fifo_a:           in     std_logic;
	----------------------------------------------------------------
	bunch_dataword_b:          in     std_logic_vector(81 downto 0);
	empty_bc_fifo_b:           in     std_logic;
	readenable_control_b:      out    std_logic;
	valid_bc_fifo_b:           in     std_logic;
	----------------------------------------------------------------
	valid_bc_data:             out    std_logic_vector(81 downto 0);
    push_valid_bc:             out    std_logic
    );	
end filter;

architecture behavioral of filter is

signal state_read:      std_logic;

begin

read_process: process(reset, clock_240)
begin
    if(reset = '1') then
        readenable_control_a    <= '0';
        readenable_control_b    <= '0';
        state_read              <= '0';
    elsif(rising_edge(clock_240)) then
        if(state_read = '0') then
            readenable_control_a    <= not(empty_bc_fifo_a);
            readenable_control_b    <= '0';
            state_read              <= '1';
        else
            readenable_control_a    <= '0';
            readenable_control_b    <= not(empty_bc_fifo_b);
            state_read              <= '0';
        end if;
    end if;
end process read_process;

valid_process: process(reset, clock_240)
begin
    if(reset = '1') then
        valid_bc_data   <= (others => '0');
        push_valid_bc   <= '0';
    elsif(rising_edge(clock_240)) then
        if(valid_bc_fifo_a = '1') then
            valid_bc_data   <= bunch_dataword_a;
            push_valid_bc   <= '1';
        elsif(valid_bc_fifo_b = '1') then
            valid_bc_data   <= bunch_dataword_b;
            push_valid_bc   <= '1';
        else
            valid_bc_data   <= (others => '0');
            push_valid_bc   <= '0';
        end if;
    end if;
end process valid_process;

end behavioral;
