----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.04.2020 16:11:00
-- Design Name: 
-- Module Name: header - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_exampledesign_package.all;

library work;
use work.top_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity header is
port
    (
    -----------------------------------------------------------------
    reset:                     in     std_logic;
	clock_240:     			   in     std_logic;
	-----------------------------------------------------------------
	signal_enable:             in     std_logic;
    general_reset:             in     std_logic;
	orbit_reset:               in     std_logic;
	orbit_valid:               in     std_logic;
	decoded_trigger:           in     std_logic_vector(79 downto 0);
	-----------------------------------------------------------------
	adc_data_in:               in     std_logic_vector(23 downto 0);
	-----------------------------------------------------------------
	push_bc_fifo:              out    std_logic;
	bc_fifo_data:              out    std_logic_vector(81 downto 0)
	-----------------------------------------------------------------
	);
end header;

architecture behavioral of header is

signal fee_orbit:           std_logic_vector(31 downto 0);
signal fee_bc:              std_logic_vector(11 downto 0);
signal orbit_incr:          std_logic;
signal count_bc:            std_logic_vector(2 downto 0);
signal bc_start:            std_logic;
signal sample_number:       std_logic_vector(2 downto 0);

signal bunch_reset:         std_logic;
signal heart_beat:          std_logic;
signal sot:                 std_logic;
signal eot:                 std_logic;
signal soc:                 std_logic;
signal eoc:                 std_logic;

signal state_soc:           std_logic;
signal run_active:          std_logic;

signal first_half_data:     std_logic;
signal second_half_data:    std_logic;

signal sample_0:            std_logic_vector(23 downto 0);
signal sample_1:            std_logic_vector(23 downto 0);
signal sample_2:            std_logic_vector(23 downto 0);
signal sample_3:            std_logic_vector(23 downto 0);
signal sample_4:            std_logic_vector(23 downto 0);
signal sample_5:            std_logic_vector(23 downto 0);

signal packet_word_1:       std_logic_vector(79 downto 0);
signal packet_word_2:       std_logic_vector(79 downto 0);
signal packet_word_3:       std_logic_vector(79 downto 0);
signal packet_word_4:       std_logic_vector(79 downto 0);


begin

bunch_reset			<= signal_enable and decoded_trigger(ORBIT_TT);															-- sync with gbt_clk (ORBIT)	
heart_beat			<= signal_enable and decoded_trigger(HB_TT);															-- sync with gbt_clk (HEART_BEAT)
sot					<= signal_enable and decoded_trigger(SOT_TT);															-- sync with gbt_clk (SOT)
eot					<= signal_enable and decoded_trigger(EOT_TT);	
soc					<= signal_enable and decoded_trigger(SOC_TT);															-- sync with gbt_clk (SOT)
eoc					<= signal_enable and decoded_trigger(EOC_TT);	

process(clock_240, reset)
begin
    if(reset = '1') then
        orbit_incr  <= '0';
    elsif(rising_edge(clock_240)) then
        if(orbit_valid = '1') then     -- ORB
            orbit_incr  <= '1';
        else
            orbit_incr  <= '0';
        end if;
    end if;
end process;

process(clock_240, reset)
begin
    if(reset = '1') then
        fee_orbit   <= (others => '0');
    elsif(rising_edge(clock_240)) then
        if(orbit_incr = '1') then
            fee_orbit   <= fee_orbit + 1;
        end if;
    end if;
end process;

process(clock_240, reset)
begin
    if(reset = '1') then
        fee_bc      <= (others => '0');
        bc_start    <= '0';
        count_bc    <= (others => '0');
    elsif(rising_edge(clock_240)) then
        if(orbit_incr = '1') then
            bc_start    <= '1';
        else
            if(count_bc = "101") then       -- counting 6 clock cycles
                bc_start    <= '1';
                count_bc    <= "000";
                fee_bc      <= fee_bc + 1;
            else
                bc_start    <= '0';
                count_bc    <= count_bc + 1;
            end if;
        end if;
     end if;
end process;

process(clock_240, reset)
begin
    if(reset = '1') then
        sample_number    <= (others => '0');
    elsif(rising_edge(clock_240)) then
        if(orbit_valid = '1') then
            sample_number   <= "001";
        elsif(sample_number = "101") then
            sample_number    <= (others => '0');
        else
            sample_number   <= sample_number + 1;
        end if;
    end if;
end process;

state_soc_process: process(reset, clock_240)
  begin
    if(reset = '1') then
        state_soc	<= '0';
        run_active	<= '0';
    elsif(rising_edge(clock_240)) then
        if(state_soc = '0') then
            if(sot = '1') then	
                run_active	<= '1';
                state_soc	<= '1';
            else	
                run_active	<= '0';
                state_soc	<= '0';
            end if;
        else
            if(eot = '1') then	
                run_active	<= '0';
                state_soc	<= '0';
            else	
                run_active	<= '1';
                state_soc	<= '1';
            end if;
        end if;
    end if;
end process state_soc_process;

process(reset, clock_240)
  begin
    if(reset = '1') then
        first_half_data     <= '0';
        second_half_data    <= '0';
    elsif(rising_edge(clock_240)) then
        if(sample_number = "001") then
            first_half_data <= '1';
        else
            first_half_data <= '0';
        end if;
        if(sample_number = "100") then
            second_half_data <= '1';
        else
            second_half_data <= '0';
        end if;
    end if;
end process;

process(reset, clock_240)
  begin
    if(reset = '1') then
        sample_0    <= (others => '0');
        sample_1    <= (others => '0');
        sample_2    <= (others => '0');
        sample_3    <= (others => '0');
    elsif(rising_edge(clock_240)) then
        if(sample_number = "000") then
            sample_0    <= adc_data_in;
        elsif(sample_number = "001") then
            sample_1    <= adc_data_in;
        elsif(sample_number = "010") then
            sample_2    <= adc_data_in;
        elsif(sample_number = "011") then
            sample_3    <= adc_data_in;
        elsif(sample_number = "100") then
            sample_4    <= adc_data_in;
        elsif(sample_number = "101") then
            sample_5    <= adc_data_in;
        end if;
    end if;
end process;

process(reset, clock_240)
  begin
    if(reset = '1') then
        packet_word_3   <= (others => '0');
        packet_word_4   <= (others => '0');
    elsif(rising_edge(clock_240)) then
        if(first_half_data = '1') then
            packet_word_3   <= sample_0 & sample_1 & adc_data_in & x"00";
        elsif(second_half_data = '1') then
            packet_word_4   <= sample_3 & sample_4 & adc_data_in & x"00";
        end if;
    end if;
end process;

process(reset, clock_240)
  begin
    if(reset = '1') then
        packet_word_1   <= (others => '0');
        packet_word_2   <= (others => '0');
    elsif(rising_edge(clock_240)) then
        if(second_half_data = '1') then
            packet_word_1   <= decoded_trigger(79 downto 0);
            packet_word_2   <= fee_orbit & fee_bc & x"000" & x"0000" & x"00";
        end if;
    end if;
end process;

process(reset, clock_240)
  begin
    if(reset = '1') then
        bc_fifo_data    <= (others => '0');
        push_bc_fifo    <= '0';
    elsif(rising_edge(clock_240)) then
        if(sample_number = "000") then
            bc_fifo_data    <= packet_word_1 & "00";
            push_bc_fifo    <= '1';
        elsif(sample_number = "001") then
            bc_fifo_data    <= packet_word_2 & "01";
            push_bc_fifo    <= '1';
        elsif(sample_number = "010") then
            bc_fifo_data    <= packet_word_2 & "10";
            push_bc_fifo    <= '1';
        elsif(sample_number = "011") then
            bc_fifo_data    <= packet_word_3 & "11";
            push_bc_fifo    <= '1';
        else
            push_bc_fifo    <= '0';
        end if;
    end if;
end process;

end behavioral;

