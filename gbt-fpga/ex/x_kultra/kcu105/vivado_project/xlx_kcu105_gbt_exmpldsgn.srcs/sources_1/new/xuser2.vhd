----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 25.02.2020 15:42:33
-- Design Name: 
-- Module Name: gbt_interface - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library work;
use work.top_pkg.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity xuser2 is
port
	(
	reset:             		   in  	  std_logic;
	clock_240:     			   in	  std_logic;         -- 240 MHz
	regs: 					   inout  REGS_RECORD;	
	-----------------------------------------------------------------
	-- trigger interface --------------------------------------------
	-----------------------------------------------------------------
	signal_enable:             out    std_logic;
    general_reset:             out    std_logic;      
	orbit_reset:               out    std_logic;
	orbit_valid:               out    std_logic;
	decoded_trigger:           out    std_logic_vector(79 downto 0);
    -----------------------------------------------------------------
	bunch_dataword3:           in     std_logic_vector(81 downto 0);
	empty_bc_fifo3:            in     std_logic;
	readenable_control3:       out    std_logic;
	valid_bc_fifo3:            in     std_logic;
	bunch_dataword2:           in     std_logic_vector(81 downto 0);
	empty_bc_fifo2:            in     std_logic;
	readenable_control2:       out    std_logic;
	valid_bc_fifo2:            in     std_logic;
	bunch_dataword1:           in     std_logic_vector(81 downto 0);
	empty_bc_fifo1:            in     std_logic;
	readenable_control1:       out    std_logic;
	valid_bc_fifo1:            in     std_logic;
	bunch_dataword0:           in     std_logic_vector(81 downto 0);
	empty_bc_fifo0:            in     std_logic;
	readenable_control0:       out    std_logic;
	valid_bc_fifo0:            in     std_logic;
	-----------------------------------------------------------------
	-- GBT FPGA interface SFP0 --------------------------------------
	-----------------------------------------------------------------
    data_to_gbt_sfp0:            out    std_logic_vector(83 downto 0);
    data_valid_to_gbt_sfp0:      out    std_logic;
    data_from_gbt_sfp0:          in     std_logic_vector(83 downto 0);                             
    data_valid_from_gbt_sfp0:    in     std_logic;            
    -----------------------------------------------------------------
	-- GBT FPGA interface SFP1 --------------------------------------
	-----------------------------------------------------------------
    data_to_gbt_sfp1:            out    std_logic_vector(83 downto 0);
    data_valid_to_gbt_sfp1:      out    std_logic;
    data_from_gbt_sfp1:          in     std_logic_vector(83 downto 0);                             
    data_valid_from_gbt_sfp1:    in     std_logic           
    -----------------------------------------------------------------
	);
end xuser2;

architecture behavioral of xuser2 is

component decoder is
port
    (
    -----------------------------------------------------------------
    reset:                      in    std_logic;
	clock_240:     			    in    std_logic;
	data_from_gbt_sfp0:         in    std_logic_vector(83 downto 0);                             
    data_valid_from_gbt_sfp0:   in    std_logic;
    -----------------------------------------------------------------
    signal_enable:              out   std_logic;
    general_reset:              out   std_logic;
    orbit_reset:                out   std_logic;
    orbit_valid:                out   std_logic;
    decoded_trigger:            out   std_logic_vector(79 downto 0)
    -----------------------------------------------------------------
	);
end component decoder;

component config is
    port
    (
    reset:             		     in  	std_logic;
	clock_240:    			     in  	std_logic;
	regs: 					     inout  REGS_RECORD;
	-- GBT FPGA interface
	-------------------------------------------------------------------	
    data_to_gbt_sfp1:            out    std_logic_vector(83 downto 0);
    data_valid_to_gbt_sfp1:      out    std_logic;
    data_from_gbt_sfp1:          in     std_logic_vector(83 downto 0);                             
    data_valid_from_gbt_sfp1:    in     std_logic
    -------------------------------------------------------------------	
    );
end component config;

component multiplexer is
port
	(
	reset:             		   in  	  std_logic;
	clock_240:     			   in	  std_logic;         -- 240 MHz
	regs: 					   in     REGS_RECORD;
	----------------------------------------------------------------
	bunch_dataword:            in     std_logic_vector(81 downto 0);
	empty_bc_fifo:             in     std_logic;
	readenable_control:        out    std_logic;
	valid_bc_fifo:             in     std_logic;
	----------------------------------------------------------------
	data_to_gbt_sfp:           out    std_logic_vector(83 downto 0);
    data_valid_to_gbt_sfp:     out    std_logic
    );
end component multiplexer;

component filter is
port
	(
	reset:             		   in  	  std_logic;
	clock_240:     			   in	  std_logic;         -- 240 MHz
	regs: 					   in     REGS_RECORD;
	----------------------------------------------------------------
	bunch_dataword_a:          in     std_logic_vector(81 downto 0);
	empty_bc_fifo_a:           in     std_logic;
	readenable_control_a:      out    std_logic;
	valid_bc_fifo_a:           in     std_logic;
	----------------------------------------------------------------
	bunch_dataword_b:          in     std_logic_vector(81 downto 0);
	empty_bc_fifo_b:           in     std_logic;
	readenable_control_b:      out    std_logic;
	valid_bc_fifo_b:           in     std_logic;
	----------------------------------------------------------------
	valid_bc_data:             out    std_logic_vector(81 downto 0);
    push_valid_bc:             out    std_logic
    );	
end component;

component bunch_crossing_fifo
  port (
    clk : in std_logic;
    rst : in std_logic;
    din : in std_logic_vector(81 downto 0);
    wr_en : in std_logic;
    rd_en : in std_logic;
    dout : out std_logic_vector(81 downto 0);
    full : out std_logic;
    empty : out std_logic;
    valid : out std_logic
  );
end component bunch_crossing_fifo;

signal valid_bc_data01:         std_logic_vector(81 downto 0);
signal push_valid_bc01:         std_logic;
signal readenable_control01:    std_logic;
signal bunch_dataword01:        std_logic_vector(81 downto 0);
signal empty_bc_fifo01:         std_logic;
signal valid_bc_fifo01:         std_logic;

signal valid_bc_data23:         std_logic_vector(81 downto 0);
signal push_valid_bc23:         std_logic;
signal readenable_control23:    std_logic;
signal bunch_dataword23:        std_logic_vector(81 downto 0);
signal empty_bc_fifo23:         std_logic;
signal valid_bc_fifo23:         std_logic;
	
begin

decoder_instance: decoder
port map
    (
    -----------------------------------------------------------------
    reset                      => reset,
	clock_240                  => clock_240,
	data_from_gbt_sfp0         => data_from_gbt_sfp0,                            
    data_valid_from_gbt_sfp0   => data_valid_from_gbt_sfp0,
    -----------------------------------------------------------------
    signal_enable              => signal_enable,
    general_reset              => general_reset,
    orbit_reset                => orbit_reset,
    orbit_valid                => orbit_valid,
    decoded_trigger            => decoded_trigger
    -----------------------------------------------------------------
	);
	
config_instance: config
    port map
    (
    reset                       => reset,
	clock_240                   => clock_240,
	-- GBT FPGA interface
	----------------------------------------------------------	
    data_to_gbt_sfp1            => open,    --data_to_gbt_sfp1,        
    data_valid_to_gbt_sfp1      => open,    --data_valid_to_gbt_sfp1,  
    data_from_gbt_sfp1          => data_from_gbt_sfp1,                                  
    data_valid_from_gbt_sfp1    => data_valid_from_gbt_sfp1
    ----------------------------------------------------------	
    );

--------------------------------------------------------------------------------------------------------------
filter_instance01: filter
port map
	(
	reset                      => reset,
	clock_240                  => clock_240,
	regs                       => regs,
	----------------------------------------------------------------
	bunch_dataword_a           => bunch_dataword0,
	empty_bc_fifo_a            => empty_bc_fifo0,
	readenable_control_a       => readenable_control0,
	valid_bc_fifo_a            => valid_bc_fifo0,
	----------------------------------------------------------------
	bunch_dataword_b           => bunch_dataword1,
	empty_bc_fifo_b            => empty_bc_fifo1,
	readenable_control_b       => readenable_control1,
	valid_bc_fifo_b            => valid_bc_fifo1,
	----------------------------------------------------------------
	valid_bc_data              => valid_bc_data01,
    push_valid_bc              => push_valid_bc01
    );	

valid_bc_fifo_instance01: bunch_crossing_fifo
  port map(
    clk             => clock_240,
    rst             => reset,
    din             => valid_bc_data01,
    wr_en           => push_valid_bc01,
    rd_en           => readenable_control01,
    dout            => bunch_dataword01,
    full            => open,
    empty           => empty_bc_fifo01,
    valid           => valid_bc_fifo01
  );
  
multiplexer_instance01: multiplexer
port map
	(
	----------------------------------------------------------------
	reset                      => reset,
	clock_240                  => clock_240,
	regs                       => regs,
	----------------------------------------------------------------
	bunch_dataword             => bunch_dataword01,
	empty_bc_fifo              => empty_bc_fifo01,
	readenable_control         => readenable_control01,
	valid_bc_fifo              => valid_bc_fifo01,
	----------------------------------------------------------------
	data_to_gbt_sfp            => data_to_gbt_sfp0,
    data_valid_to_gbt_sfp      => data_valid_to_gbt_sfp0
    ----------------------------------------------------------------
    );	
  
--------------------------------------------------------------------------------------------------------------
filter_instance23: filter
port map
	(
	reset                      => reset,
	clock_240                  => clock_240,
	regs                       => regs,
	----------------------------------------------------------------
	bunch_dataword_a           => bunch_dataword2,
	empty_bc_fifo_a            => empty_bc_fifo2,
	readenable_control_a       => readenable_control2,
	valid_bc_fifo_a            => valid_bc_fifo2,
	----------------------------------------------------------------
	bunch_dataword_b           => bunch_dataword3,
	empty_bc_fifo_b            => empty_bc_fifo3,
	readenable_control_b       => readenable_control3,
	valid_bc_fifo_b            => valid_bc_fifo3,
	----------------------------------------------------------------
	valid_bc_data              => valid_bc_data23,
    push_valid_bc              => push_valid_bc23
    );	

valid_bc_fifo_instance23: bunch_crossing_fifo
  port map(
    clk             => clock_240,
    rst             => reset,
    din             => valid_bc_data23,
    wr_en           => push_valid_bc23,
    rd_en           => readenable_control23,
    dout            => bunch_dataword23,
    full            => open,
    empty           => empty_bc_fifo23,
    valid           => valid_bc_fifo23
  );
  
multiplexer_instance23: multiplexer
port map
	(
	----------------------------------------------------------------
	reset                      => reset,
	clock_240                  => clock_240,
	regs                       => regs,
	----------------------------------------------------------------
	bunch_dataword             => bunch_dataword23,
	empty_bc_fifo              => empty_bc_fifo23,
	readenable_control         => readenable_control23,
	valid_bc_fifo              => valid_bc_fifo23,
	----------------------------------------------------------------
	data_to_gbt_sfp            => data_to_gbt_sfp1,
    data_valid_to_gbt_sfp      => data_valid_to_gbt_sfp1
    ----------------------------------------------------------------
    );
    
	
end behavioral;
