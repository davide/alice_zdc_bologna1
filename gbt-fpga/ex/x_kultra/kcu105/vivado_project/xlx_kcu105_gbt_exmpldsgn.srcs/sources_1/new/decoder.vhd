----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03.04.2020 10:58:11
-- Design Name: 
-- Module Name: decoder - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity decoder is
port
    (
    -----------------------------------------------------------------
    reset:                      in    std_logic;
	clock_240:     			    in    std_logic;
	data_from_gbt_sfp0:         in    std_logic_vector(83 downto 0);                             
    data_valid_from_gbt_sfp0:   in    std_logic;
    -----------------------------------------------------------------
    signal_enable:              out   std_logic;
    general_reset:              out   std_logic;
    orbit_reset:                out   std_logic;
    orbit_valid:                out   std_logic;
    decoded_trigger:            out   std_logic_vector(79 downto 0)
    -----------------------------------------------------------------
	);
end decoder;

architecture behavioral of decoder is

begin

signal_enable       <= data_valid_from_gbt_sfp0;
general_reset       <= data_from_gbt_sfp0(12);
orbit_reset         <= data_from_gbt_sfp0(0);
orbit_valid         <= '1';     -- ?????????
decoded_trigger     <= data_from_gbt_sfp0(79 downto 0);

end behavioral;
