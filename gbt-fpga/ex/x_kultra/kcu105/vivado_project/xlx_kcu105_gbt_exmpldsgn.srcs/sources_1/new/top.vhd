----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.02.2020 09:39:41
-- Design Name: 
-- Module Name: top - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;

-- Xilinx devices library:
library unisim;
use unisim.vcomponents.all;

-- Custom libraries and packages:
use work.gbt_bank_package.all;
use work.vendor_specific_gbt_bank_package.all;
use work.gbt_exampledesign_package.all;

library work;
use work.top_pkg.all;

entity top is
    port
    (
    reset:                      in  std_logic; 
    SYSCLK_P:                   in  std_logic;      -- 300 MHz
    SYSCLK_N:                   in  std_logic;
    USER_CLOCK_P:               in  std_logic;      -- 125 MHz
    USER_CLOCK_N:               in  std_logic;
    SMA_MGT_REFCLK_P:           in  std_logic;      -- 120 MHz
    SMA_MGT_REFCLK_N:           in  std_logic;
    SFP0_TX_P:                  out std_logic;
    SFP0_TX_N:                  out std_logic;
    SFP0_RX_P:                  in  std_logic;
    SFP0_RX_N:                  in  std_logic;     
    SFP1_TX_P:                  out std_logic;
    SFP1_TX_N:                  out std_logic;
    SFP1_RX_P:                  in  std_logic;
    SFP1_RX_N:                  in  std_logic;       
    SFP0_TX_DISABLE:            out std_logic;
    SFP1_TX_DISABLE:            out std_logic;
    USER_SMA_GPIO_P:            out std_logic;    
    USER_SMA_GPIO_N:            out std_logic
    );
end top;

architecture behavioral of top is

signal sysclk:                      std_logic;
signal user_clock:                  std_logic;
signal regs: 						REGS_RECORD; 

signal data_to_gbt_sfp0:            std_logic_vector(83 downto 0);
signal data_valid_to_gbt_sfp0:      std_logic;
signal data_from_gbt_sfp0:          std_logic_vector(83 downto 0);                             
signal data_valid_from_gbt_sfp0:    std_logic;
signal clock_gbt_rx_sfp0:           std_logic;

signal data_to_gbt_sfp1:            std_logic_vector(83 downto 0);
signal data_valid_to_gbt_sfp1:      std_logic;
signal data_from_gbt_sfp1:          std_logic_vector(83 downto 0);                             
signal data_valid_from_gbt_sfp1:    std_logic;
signal clock_gbt_rx_sfp1:           std_logic;

signal gbt_data_fifo_dti_adc_ck:    std_logic_vector(64 downto 0);
signal gbt_data_fifo_wr_adc_ck:     std_logic;
signal gbt_data_fifo_almfull:	    std_logic;

signal clock_240:                   std_logic;

signal adc0_data_out:               std_logic_vector(23 downto 0);
signal adc1_data_out:               std_logic_vector(23 downto 0);
signal adc2_data_out:               std_logic_vector(23 downto 0);
signal adc3_data_out:               std_logic_vector(23 downto 0);

signal signal_enable:               std_logic;
signal general_reset:               std_logic;
signal orbit_reset:                 std_logic;
signal orbit_valid:                 std_logic;
signal decoded_trigger:             std_logic_vector(79 downto 0);
signal bunch_dataword3:             std_logic_vector(81 downto 0);
signal empty_bc_fifo3:              std_logic;
signal readenable_control3:         std_logic;
signal valid_bc_fifo3:              std_logic;
signal bunch_dataword2:             std_logic_vector(81 downto 0);
signal empty_bc_fifo2:              std_logic;
signal readenable_control2:         std_logic;
signal valid_bc_fifo2:              std_logic;
signal bunch_dataword1:             std_logic_vector(81 downto 0);
signal empty_bc_fifo1:              std_logic;
signal readenable_control1:         std_logic;
signal valid_bc_fifo1:              std_logic;
signal bunch_dataword0:             std_logic_vector(81 downto 0);
signal empty_bc_fifo0:              std_logic;
signal readenable_control0:         std_logic;
signal valid_bc_fifo0:              std_logic;

component clk_wiz_0
port
    (
    clk_in1:    in  std_logic;
    reset:      in  std_logic;
    clk_out1:   out std_logic;
    locked:     out std_logic
    ); 
end component clk_wiz_0;
    
component gbtfpga is
    port
    (
    reset:                      in  std_logic; 
    sysclk:                     in  std_logic;  -- 300 MHz
    user_clock:                 in  std_logic;  -- 125 MHz
    SMA_MGT_REFCLK_P:           in  std_logic;  -- 120 MHz
    SMA_MGT_REFCLK_N:           in  std_logic;
    SFP0_TX_P:                  out std_logic;
    SFP0_TX_N:                  out std_logic;
    SFP0_RX_P:                  in  std_logic;
    SFP0_RX_N:                  in  std_logic;     
    SFP1_TX_P:                  out std_logic;
    SFP1_TX_N:                  out std_logic;
    SFP1_RX_P:                  in  std_logic;
    SFP1_RX_N:                  in  std_logic;       
    SFP0_TX_DISABLE:            out std_logic;
    SFP1_TX_DISABLE:            out std_logic;
    ----------------------------------------------------------
    data_to_gbt_sfp0:           in  std_logic_vector(83 downto 0);
    data_valid_to_gbt_sfp0:     in  std_logic;
    data_from_gbt_sfp0:         out std_logic_vector(83 downto 0);                             
    data_valid_from_gbt_sfp0:   out std_logic;                        
    clock_gbt_rx_sfp0:          out std_logic;
  
    ----------------------------------------------------------
    data_to_gbt_sfp1:           in  std_logic_vector(83 downto 0);
    data_valid_to_gbt_sfp1:     in  std_logic;
    data_from_gbt_sfp1:         out std_logic_vector(83 downto 0);                             
    data_valid_from_gbt_sfp1:   out std_logic;                        
    clock_gbt_rx_sfp1:          out std_logic;
    ----------------------------------------------------------
    USER_SMA_GPIO_P:            out std_logic;    
    USER_SMA_GPIO_N:            out std_logic
    );
end component gbtfpga;

component xuser2 is
port
	(
	reset:             		   in  	  std_logic;
	clock_240:     			   in	  std_logic;         -- 240 MHz
	regs: 					   inout  REGS_RECORD;	
	-----------------------------------------------------------------
	-- trigger interface --------------------------------------------
	-----------------------------------------------------------------
	signal_enable:             out    std_logic;
    general_reset:             out    std_logic;      
	orbit_reset:               out    std_logic;
	orbit_valid:               out    std_logic;
	decoded_trigger:           out    std_logic_vector(79 downto 0);
    -----------------------------------------------------------------
	bunch_dataword3:           in     std_logic_vector(81 downto 0);
	empty_bc_fifo3:            in     std_logic;
	readenable_control3:       out    std_logic;
	valid_bc_fifo3:            in     std_logic;
	bunch_dataword2:           in     std_logic_vector(81 downto 0);
	empty_bc_fifo2:            in     std_logic;
	readenable_control2:       out    std_logic;
	valid_bc_fifo2:            in     std_logic;
	bunch_dataword1:           in     std_logic_vector(81 downto 0);
	empty_bc_fifo1:            in     std_logic;
	readenable_control1:       out    std_logic;
	valid_bc_fifo1:            in     std_logic;
	bunch_dataword0:           in     std_logic_vector(81 downto 0);
	empty_bc_fifo0:            in     std_logic;
	readenable_control0:       out    std_logic;
	valid_bc_fifo0:            in     std_logic;
	-----------------------------------------------------------------
	-- GBT FPGA interface SFP0 --------------------------------------
	-----------------------------------------------------------------
    data_to_gbt_sfp0:            out    std_logic_vector(83 downto 0);
    data_valid_to_gbt_sfp0:      out    std_logic;
    data_from_gbt_sfp0:          in     std_logic_vector(83 downto 0);                             
    data_valid_from_gbt_sfp0:    in     std_logic;            
    -----------------------------------------------------------------
	-- GBT FPGA interface SFP1 --------------------------------------
	-----------------------------------------------------------------
    data_to_gbt_sfp1:            out    std_logic_vector(83 downto 0);
    data_valid_to_gbt_sfp1:      out    std_logic;
    data_from_gbt_sfp1:          in     std_logic_vector(83 downto 0);                             
    data_valid_from_gbt_sfp1:    in     std_logic           
    -----------------------------------------------------------------
	);
end component xuser2;

component xuser1 is
    port
    (
    -----------------------------------------------------------------
    reset:                     in     std_logic;
	clock_240:     			   in     std_logic;
	-----------------------------------------------------------------
	signal_enable:             in     std_logic;
    general_reset:             in     std_logic;
	orbit_reset:               in     std_logic;
	orbit_valid:               in     std_logic;
	decoded_trigger:           in     std_logic_vector(79 downto 0);
	-----------------------------------------------------------------
	adc3_data_in:              in     std_logic_vector(23 downto 0);
    adc2_data_in:              in     std_logic_vector(23 downto 0);
    adc1_data_in:              in     std_logic_vector(23 downto 0);
    adc0_data_in:              in     std_logic_vector(23 downto 0); 
    -----------------------------------------------------------------
	bunch_dataword3:           out    std_logic_vector(81 downto 0);
	readenable_control3:       in     std_logic;
	bunch_dataword2:           out    std_logic_vector(81 downto 0);
	readenable_control2:       in     std_logic;
	bunch_dataword1:           out    std_logic_vector(81 downto 0);
	readenable_control1:       in     std_logic;
	bunch_dataword0:           out    std_logic_vector(81 downto 0);
	readenable_control0:       in     std_logic
	-----------------------------------------------------------------
    );		
end component xuser1;

component fmc1 is
port
    (
    clock_240:      in      std_logic;
    reset:          in      std_logic;
    adc3_data_out:  out     std_logic_vector(23 downto 0);
    adc2_data_out:  out     std_logic_vector(23 downto 0);
    adc1_data_out:  out     std_logic_vector(23 downto 0);
    adc0_data_out:  out     std_logic_vector(23 downto 0)   
    );
end component fmc1;

begin

clock_240   <= clock_gbt_rx_sfp0;   -- 240 MHz clock from GBT RX frame clock

   -- Fabric clock:
   ----------------
   
   -- Comment: USER_CLOCK frequency: 125 MHz 
   
   userClockIbufgds: ibufgds
      generic map (
         IBUF_LOW_PWR                            => FALSE,      
         IOSTANDARD                              => "LVDS_25")
      port map 
      (     
         O                                       => user_clock,   
         I                                       => USER_CLOCK_P,  
         IB                                      => USER_CLOCK_N 
      );
      
    sysclk_inst: ibufds
     port map 
     (
        I                                        => SYSCLK_P,
        IB                                       => SYSCLK_N,
        O                                        => sysclk
     ); 

gbtfpga_instance:  gbtfpga
    port map
    (
    reset                       => reset,
    sysclk                      => sysclk,
    user_clock                  => user_clock,
    SMA_MGT_REFCLK_P            => SMA_MGT_REFCLK_P,
    SMA_MGT_REFCLK_N            => SMA_MGT_REFCLK_N,
    SFP0_TX_P                   => SFP0_TX_P,       
    SFP0_TX_N                   => SFP0_TX_N,      
    SFP0_RX_P                   => SFP0_RX_P,       
    SFP0_RX_N                   => SFP0_RX_N,       
    SFP1_TX_P                   => SFP1_TX_P,       
    SFP1_TX_N                   => SFP1_TX_N,       
    SFP1_RX_P                   => SFP1_RX_P,       
    SFP1_RX_N                   => SFP1_RX_N,            
    SFP0_TX_DISABLE             => SFP0_TX_DISABLE, 
    SFP1_TX_DISABLE             => SFP1_TX_DISABLE, 
    ----------------------------------------------------------
    data_to_gbt_sfp0            => data_to_gbt_sfp0,
    data_valid_to_gbt_sfp0      => data_valid_to_gbt_sfp0,
    data_from_gbt_sfp0          => data_from_gbt_sfp0,                            
    data_valid_from_gbt_sfp0    => data_valid_from_gbt_sfp0,                       
    clock_gbt_rx_sfp0           => clock_gbt_rx_sfp0,
  
    ----------------------------------------------------------
    data_to_gbt_sfp1            => data_to_gbt_sfp1,        
    data_valid_to_gbt_sfp1      => data_valid_to_gbt_sfp1,  
    data_from_gbt_sfp1          => data_from_gbt_sfp1,                                  
    data_valid_from_gbt_sfp1    => data_valid_from_gbt_sfp1,                       
    clock_gbt_rx_sfp1           => clock_gbt_rx_sfp1,       
    ----------------------------------------------------------
    USER_SMA_GPIO_P             => USER_SMA_GPIO_P,  
    USER_SMA_GPIO_N             => USER_SMA_GPIO_N
    );

xuser2_instance: xuser2
    port map
	(
	reset                      => reset,
	clock_240                  => clock_240,
	regs                       => regs,
	-----------------------------------------------------------------
	-- trigger interface --------------------------------------------
	-----------------------------------------------------------------
	signal_enable              => signal_enable,
	general_reset              => general_reset,
	orbit_reset                => orbit_reset,
	orbit_valid                => orbit_valid,
	decoded_trigger            => decoded_trigger,
    -----------------------------------------------------------------
	bunch_dataword3            => bunch_dataword3,
	empty_bc_fifo3             => empty_bc_fifo3,
	readenable_control3        => readenable_control3,
	valid_bc_fifo3             => valid_bc_fifo3,
	bunch_dataword2            => bunch_dataword2,
	empty_bc_fifo2             => empty_bc_fifo2,
	readenable_control2        => readenable_control2,
	valid_bc_fifo2             => valid_bc_fifo2,
	bunch_dataword1            => bunch_dataword1,
	empty_bc_fifo1             => empty_bc_fifo1,
	readenable_control1        => readenable_control1,
	valid_bc_fifo1             => valid_bc_fifo1,
	bunch_dataword0            => bunch_dataword0,
	empty_bc_fifo0             => empty_bc_fifo0,
	readenable_control0        => readenable_control0,
	valid_bc_fifo0             => valid_bc_fifo0,
	-----------------------------------------------------------------
	-- GBT FPGA interface SFP0 --------------------------------------
	-----------------------------------------------------------------
    data_to_gbt_sfp0           => data_to_gbt_sfp0,
    data_valid_to_gbt_sfp0     => data_valid_to_gbt_sfp0,
    data_from_gbt_sfp0         => data_from_gbt_sfp0,                        
    data_valid_from_gbt_sfp0   => data_valid_from_gbt_sfp0,        
    -----------------------------------------------------------------
	-- GBT FPGA interface SFP1 --------------------------------------
	-----------------------------------------------------------------
    data_to_gbt_sfp1           => data_to_gbt_sfp1,
    data_valid_to_gbt_sfp1     => data_valid_to_gbt_sfp1,
    data_from_gbt_sfp1         => data_from_gbt_sfp1,                        
    data_valid_from_gbt_sfp1   => data_valid_from_gbt_sfp1       
    -----------------------------------------------------------------
	);

xuser1_instance: xuser1
    port map
    (
    -----------------------------------------------------------------
    reset                      => reset,
	clock_240                  => clock_240,
	-----------------------------------------------------------------
	signal_enable              => signal_enable,
	general_reset              => general_reset,
	orbit_reset                => orbit_reset,
	orbit_valid                => orbit_valid,
	decoded_trigger            => decoded_trigger,
	adc3_data_in               => adc3_data_out,
    adc2_data_in               => adc2_data_out,
    adc1_data_in               => adc1_data_out,
    adc0_data_in               => adc0_data_out,
    -----------------------------------------------------------------
	bunch_dataword3            => bunch_dataword3,
	readenable_control3        => readenable_control3,
	bunch_dataword2            => bunch_dataword2,
	readenable_control2        => readenable_control2,
	bunch_dataword1            => bunch_dataword1,
	readenable_control1        => readenable_control1,
	bunch_dataword0            => bunch_dataword0,
	readenable_control0        => readenable_control0
	-----------------------------------------------------------------
    );

fmc1_instance: fmc1
port map
    (
    clock_240       => clock_240,
    reset           => reset,
    adc3_data_out   => adc3_data_out,
    adc2_data_out   => adc2_data_out,
    adc1_data_out   => adc1_data_out,
    adc0_data_out   => adc0_data_out
    );
    
end behavioral;
