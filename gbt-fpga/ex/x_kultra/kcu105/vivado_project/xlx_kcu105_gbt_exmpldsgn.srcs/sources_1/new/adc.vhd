----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 27.03.2020 18:26:37
-- Design Name: 
-- Module Name: adc - behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity adc is
port
    (
    clock_240:      in      std_logic;                          -- 240 MHz clock
    reset:          in      std_logic;
    adc_data_out:   out     std_logic_vector(23 downto 0)
    );
end adc;

architecture behavioral of adc is

component prom is
  port (
    clka:   in  std_logic;
    ena:    in  std_logic;
    addra:  in  std_logic_vector(7 downto 0);
    douta:  out std_logic_vector(23 downto 0)
  );
end component prom;

signal prom_enable:     std_logic;
signal prom_address:    std_logic_vector(7 downto 0);
signal prom_data:       std_logic_vector(23 downto 0);

begin

prom_instance: prom
  port map(
    clka    => clock_240,
    ena     => prom_enable,
    addra   => prom_address,
    douta   => prom_data
  );
  
process(clock_240, reset)
begin
    if(reset = '1') then
        prom_enable     <= '0';
        prom_address    <= (others => '0');
    elsif(rising_edge(clock_240)) then
        prom_enable     <= '1';
        prom_address    <= prom_address + 1;
    end if;
end process;

adc_data_out    <= prom_data;

end behavioral;
